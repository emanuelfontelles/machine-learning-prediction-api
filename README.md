# Prediction REST API

This repository provides a web server REST API to make predictions using pre-trained
models.

In this case, we presented here a Catboost Classifier using variables provide in `prediction_with_api.api.customer.credit.schema`. This variables are used to predict probabilities of default (in this case over45mob6).

## Data Sources

Some models, like Customer's credit model, uses external data stored in S3 generated
by an ETL.

## Development

### Requirements

- Poetry
- [Git LFS](https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/)

### Testing

```bash
poetry shell
# Install the project and run the tests.
make check
# Run the web server locally in "development" mode.
prediction-rest-server
```

## Benchmarking

- To implemment
