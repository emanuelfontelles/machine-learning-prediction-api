SRC_DIR := prediction_with_api
REGISTRY_PREFIX := registry.gitlab.com/emanuelfontelles/prediction-with-api
LAST_GIT_HASH := $(shell git rev-parse --short HEAD)

.PHONY: ci-img-update
ci-img-update:
	DOCKER_BUILDKIT=1 docker build \
	    --target ci-img \
	    -t $(REGISTRY_PREFIX):ci-$(LAST_GIT_HASH) .
	docker push $(REGISTRY_PREFIX):ci-$(LAST_GIT_HASH)


.PHONY: check
check:
	poetry install -n
	poetry run mypy --ignore-missing-imports $(SRC_DIR) tests
	poetry run flake8 --ignore=E731,W503 $(SRC_DIR) tests
	APP_SETTINGS=testing poetry run pytest \
		     --cov-report term-missing \
		     --cov=$(SRC_DIR) \
		     tests
