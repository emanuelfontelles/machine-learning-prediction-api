from datetime import timedelta
from freezegun import freeze_time
from prediction_with_api._utils import cache_with_schedule


def test_cache_with_schedule():
    n_calls = 0
    with freeze_time("2020-03-22 08:10:00") as fdt:

        @cache_with_schedule("0 9 * * *")
        def cached_func(x: int) -> dict:
            nonlocal n_calls
            n_calls += 1
            return {"a": x, "b": 20}

        assert cached_func(10) == {"a": 10, "b": 20}
        assert n_calls == 1

        # Cached call.
        fdt.tick(timedelta(minutes=30))
        assert cached_func(10) == {"a": 10, "b": 20}
        assert n_calls == 1

        # Cache expired.
        fdt.tick(timedelta(minutes=30))
        assert cached_func(10) == {"a": 10, "b": 20}
        assert n_calls == 2
        assert cached_func(10) == {"a": 10, "b": 20}
        assert n_calls == 2

        # Cached call.
        fdt.tick(timedelta(hours=12))
        assert cached_func(10) == {"a": 10, "b": 20}
        assert n_calls == 2

        # New argument, not cached yet.
        assert cached_func(11) == {"a": 11, "b": 20}
        assert n_calls == 3

        # Cache expired.
        fdt.tick(timedelta(hours=12))
        assert cached_func(10) == {"a": 10, "b": 20}
        assert n_calls == 4
