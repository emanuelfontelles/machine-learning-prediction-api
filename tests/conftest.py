import pytest
from flask import Flask
from prediction_with_api.app import create_app


@pytest.fixture(scope="session")
def test_app() -> Flask:
    app = create_app()
    return app


@pytest.fixture
def test_client(test_app):
    with test_app.test_client() as tclient:
        yield tclient
