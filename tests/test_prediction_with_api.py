import pytest
import requests
import signal
import subprocess
from time import sleep
from prediction_with_api.api.customer import CREDIT_ENDPOINT


ENDPOINT_PYTEST_PARAMS = ("endpoint", [CREDIT_ENDPOINT])


@pytest.fixture
def server():
    proc = subprocess.Popen(
        ["prediction-rest-server", "--workers", "1", "--port", "5050"],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )

    sleep(4)
    yield

    proc.send_signal(signal.SIGINT)
    proc.wait()
    for line in proc.stdout:
        print(line.strip())


@pytest.mark.skip()
@pytest.mark.parametrize(*ENDPOINT_PYTEST_PARAMS)
def test_http_server(endpoint, server):
    headers = {
        "content-type": "application/json",
        "authorization": "Basic bGVuZGljbzpkZXY=",
    }
    resp = requests.post(f"http://localhost:5050/{endpoint}", headers=headers, json={})
    assert resp.status_code == 400, resp.json()

    resp = requests.get(f"http://localhost:5050/{endpoint}", headers=headers)
    assert resp.status_code == 200, resp.json()
