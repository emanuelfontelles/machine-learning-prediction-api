#!/bin/bash

set -eou pipefail

poetry run prediction-rest-server $@
