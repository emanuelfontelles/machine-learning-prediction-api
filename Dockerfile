FROM python:3.7-slim AS base-img

ENV POETRY_VERSION=1.0.2
ENV PATH="/root/.poetry/bin:$PATH"

RUN apt-get update -y \
    && apt-get install -y --no-install-recommends curl make jq \
        # bjoern dependencies
        gcc libev-dev libc6-dev \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*


# Installing poetry.
RUN curl -sSL \
        https://raw.githubusercontent.com/python-poetry/poetry/"$POETRY_VERSION"/get-poetry.py \
    | python

WORKDIR /app

# Install basic dependencies.
COPY pyproject.toml poetry.lock ./
RUN poetry install -n --no-dev --no-root

###############################
# CI image
###############################
FROM base-img as ci-img

RUN apt-get update -y \
    && apt-get install -y --no-install-recommends awscli \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

RUN poetry install -n --no-root

###############################
# Production image
###############################
FROM base-img as prd-img

COPY ./prediction_with_api /app/prediction_with_api
RUN poetry install -n --no-dev

COPY ./docker_entrypoint.sh /app/docker_entrypoint.sh
ENTRYPOINT ["/app/docker_entrypoint.sh"]
