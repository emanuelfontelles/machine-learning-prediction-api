from flask import Blueprint
from flask_restful import Api
from prediction_with_api.configuration import get_config
from prediction_with_api.api.customer._preprocessing import read_async_data
from prediction_with_api.api.customer.credit.predict import (
    CreditModel,
    CREDIT_ENDPOINT,
)


def get_blueprint() -> Blueprint:
    customer_blueprint = Blueprint("customer", __name__)
    api = Api(customer_blueprint, catch_all_404s=True)
    api.add_resource(CreditModel, CREDIT_ENDPOINT)
    return customer_blueprint
