from functools import reduce
from numpy import nan
from pandas import DataFrame, to_datetime

INDEX_KEY = "user_uuid"

RENAMING_MAP = {
    "bacen_emprestimocredrotativocartaocredito_sincrono": "bacen_emprestimocredrotativocartaocreditodatabaseatual_sincrono",  # noqa: [E501]
    "bacen_financiamentovalortotal_sincrono": "bacen_financiamentovalortotaldatabaseatual_sincrono",  # noqa: [E501]
    "user_has_loan_assincrono": "has_previous_loan_sincrono",
    "bacen_emprestimochequeespecial_sincrono": "bacen_emprestimochequeespecialdatabaseatual_sincrono",  # noqa: [E501]
    "bacen_limitecredito_sincrono": "bacen_limitecreditodatabaseatual_sincrono",  # noqa: [E501]
    "bacen_emprestimocartaocreditocomprafatparcinstfinancrt_sincrono": "bacen_emprestimocartaocreditocomprafatparcinstfinancrtdatabaseatual_sincrono",  # noqa: [E501]
    "bacen_emprestimopessoalcomconsiginacao_sincrono": "bacen_emprestimopessoalcomconsiginacaodatabaseatual_sincrono",  # noqa: [E501]
    "bacen_emprestimovalortotal_sincrono": "bacen_emprestimovalortotaldatabaseatual_sincrono",  # noqa: [E501]
    "gtwr_serasab49c_titular_cidade_assincrono": "gtwr_serasab49c_titular_cidade.1_assincrono",  # noqa: [E501]
}


def _fix_rename_date(
    df: DataFrame,
    col: str,
    prefix: str = "new_",
    refcol: str = "approval_created_dt_sincrono",
) -> DataFrame:
    """
    Removes malformed date values, makes imputation of missing values and
    converts dates to number of days relative the approval date.
    """
    new_col = prefix + col
    # Removing malformed date values like "00/0000".
    df[col].replace({"00/0000": None, "00/00/0000": None}, inplace=True)
    # Creating a new column to replace the original.
    col_non_null = df[col][df[col].notnull()].apply(to_datetime)
    df[new_col] = (
        nan
        if len(col_non_null) < 1
        else (df[refcol].apply(to_datetime) - col_non_null).dt.days
    )
    # Replacing missing values of the new column.
    df[new_col].fillna(0.0, inplace=True)
    # Forcing a specific type to data.
    df[new_col] = df[new_col].astype("float64", copy=False)
    # Dropping the original column.
    df.drop(columns=[col], inplace=True)
    return df


def _convert_to_weird_name(col: str) -> str:
    """
    Unnecessary but necessary renaming.

    This renaming is necessary because the model was trained with these names,
    but it does not make anything simpler.  I don't know why it was done like
    this...
    """
    if col.startswith("bacen") or col.startswith("new_bacen"):
        return col.replace("bacen", "gtwr_bacen_titular")
    elif col.startswith("serasa") or col.startswith("new_serasa"):
        return col.replace("serasa", "gtwr_serasab49c_titular")
    else:
        return col


def _fill_integer_col(
    df: DataFrame, col: str, default_val: int = 0, astype: str = "int64"
) -> None:
    """
    Mutates the given Dataframe by filling missing values with "default_val"
    and casting the type to "astype".
    """
    df[col].fillna(default_val, inplace=True)
    df[col] = df[col].astype(astype, copy=False)


def mutate_preprocessing(df: DataFrame) -> DataFrame:
    """
    Applies preprocessing routines INPLACE (mutation).
    """
    # Rename columns, use lower case for all names.
    df.rename(str.lower, axis="columns", inplace=True)
    # Add a new feature: "idade".
    get_year = lambda col: df[col].apply(to_datetime).dt.year
    df["idade"] = get_year("approval_created_dt_sincrono") - get_year(
        "approval_birth_month_sincrono"
    )
    # Processing columns with dates.
    df = reduce(
        lambda rdf, col: _fix_rename_date(rdf, col),
        [
            "serasa_dataadmissao_sincrono",
            "bacen_datainiciorelacionamentoif_sincrono",
            "serasa_registroconsultasdataatual_sincrono",
            "gtwr_serasab49c_titular_registroconsultasdataatual_assincrono",
        ],
        df,
    )
    # Filling missing values of text columns.
    # PS.: Trying to apply "fillna" to multiple columns at once didn't work.
    for c in df.select_dtypes(include="object").columns:
        df[c].fillna("NA", inplace=True)
    # Filling missing values for a specific integer column. The model expects
    # it to be 1 or 0, but the data is comming with "NULL" to represent 0.
    _fill_integer_col(df, "user_has_loan_assincrono")
    # Unnecessary but necessary renaming.
    df.rename(columns=RENAMING_MAP, inplace=True)
    df.rename(_convert_to_weird_name, axis="columns", inplace=True)
    return df
