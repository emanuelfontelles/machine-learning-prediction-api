from pandas import DataFrame
from typing import Tuple
from catboost import CatBoostClassifier
from prediction_with_api.api.customer._preprocessing import load_catboost_classifier
from prediction_with_api.api.customer.credit._credit_resource import (
    customerCreditResource,
)
from prediction_with_api.api.customer.credit._schema import sync_credit_schema
from prediction_with_api.api.customer.credit._preprocessing import INDEX_KEY


CREDIT_VERSION = "v1"
CREDIT_ENDPOINT = "-".join(["/customer-credit", CREDIT_VERSION])

CREDIT_MODEL_VERSION = "20200325"
credit_model = load_catboost_classifier(
    f"./prediction_with_api/ml_models/catBoost_v20_2_credit_{CREDIT_MODEL_VERSION}"  # noqa: [E501]
)


CREDIT_SCORE_FACTOR = {
    (975, 1000): "L1",
    (950, 975): "L2",
    (922, 950): "L3",
    (909, 922): "L4",
    (875, 909): "L5",
    (837, 875): "L6",
    (725, 837): "L7",
    (572, 725): "L8",
    (499, 572): "L9",
    (399, 499): "L10",
    (338, 399): "L11",
    (0, 338): "L12",
}


def get_credit_score(
    df: DataFrame, model: CatBoostClassifier, credit_factor: dict
) -> Tuple[int, Tuple[str, str], str]:
    score = round(1000 * model.predict_proba(df[model.feature_names_])[0, 0])

    score_bin, score_factor_value = next(
        (interval, factor)
        for interval, factor in credit_factor.items()
        if interval[0] < score <= interval[1]
    )

    return score, score_bin, score_factor_value


class customerCreditModel(customerCreditResource):
    def __init__(self):
        super(customerCreditResource, self).__init__(sync_credit_schema)

    def score(self, df: DataFrame) -> dict:
        score, _, _ = get_credit_score(df, credit_model, CREDIT_SCORE_FACTOR)

        return {
            "message": "Successful prediction",
            "results": {
                "score": score,
                "version": CREDIT_MODEL_VERSION,
                "type": "credit",
                INDEX_KEY: df[INDEX_KEY][0],
            },
        }
