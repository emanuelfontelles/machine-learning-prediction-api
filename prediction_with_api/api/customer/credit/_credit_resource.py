from flask import request
from loguru import logger as log
from marshmallow import ValidationError
from pandas import DataFrame
from prediction_with_api.configuration import get_config
from prediction_with_api.api.customer._base_resource import customerBaseResource
from prediction_with_api.api.customer._preprocessing import (
    read_async_data,
    join_sync_async_data,
)
from prediction_with_api.api.customer.credit._preprocessing import mutate_preprocessing


class customerCreditResource(customerBaseResource):
    def __init__(self, _schema):
        super(customerCreditResource, self).__init__(_schema)

    def post(self):
        try:
            raw_sync_data = self.schema.load(request.get_json())
        except ValidationError as er:
            return {"message": er.messages.get("message", er.messages)}, 400
        log.debug(f"Sync input data: {raw_sync_data}")

        try:
            async_data = read_async_data(get_config().customer_ASYNC_DATA_PATH)
        except Exception:
            log.exception("customer async data could not be read")
            return {"message": "[E10] Async dataset could not be read"}, 500
        # Complete DataFrame containing both "sync" and "async" data about a
        # "user_uuid". It must have a single record.
        join_df = join_sync_async_data(raw_sync_data, async_data)
        # Preprocessing the joined DF.
        df: DataFrame = mutate_preprocessing(join_df)
        return self.score(df)
