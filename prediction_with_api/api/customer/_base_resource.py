from flask_restful import Resource
from pandas import DataFrame
from prediction_with_api.api._utils import gen_authorization_wrapper
from prediction_with_api.configuration import get_config
from prediction_with_api.api._utils import describe_schema


class customerBaseResource(Resource):
    def __init__(self, schema):
        self.schema = schema

    method_decorators = [
        gen_authorization_wrapper("customer", get_config().customer_AUTH_TOKEN)
    ]

    def get(self):
        return (
            {
                "message": "Input schema",
                "results": describe_schema(self.schema),
            },
            200,
        )

    def post(self):
        raise NotImplementedError(
            "The https post verb to data throw API is not implemented"
        )

    def score(self, df: DataFrame) -> dict:
        raise NotImplementedError("The function to score the data is not implemented")
