from flask import request, jsonify
from functools import wraps
from loguru import logger as log
from marshmallow import Schema
from marshmallow.fields import Field
from typing import Callable, Dict


def gen_authorization_wrapper(username: str, token: str) -> Callable:
    """
    Generates a decorator for authorization of Flask-RestFul resources.
    """

    def authorize(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            auth = request.authorization or {}
            log.debug(
                f"Checking authorization for user {auth.get('username')}"
            )
            if username == auth.get("username") and token == auth.get(
                "password"
            ):
                return f(*args, **kwargs)
            else:
                resp = jsonify({"message": "Unauthorized. Use Basic auth."})
                resp.status_code = 401
                resp.headers.add(
                    "WWW-Authenticate", 'Basic realm="Prediction API"'
                )
                return resp

        return wrapper

    return authorize


def describe_schema(schema_obj: Schema) -> Dict[str, dict]:
    def conv_field(field: Field) -> dict:
        return {
            "required": field.required,
            "nullable": field.allow_none,
            "type": field.__class__.__name__,
            "info": field.metadata
        }

    return {
        fname: conv_field(field) for fname, field in schema_obj.fields.items()
    }
