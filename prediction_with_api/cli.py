import bjoern
import fire
from loguru import logger as log
from multiprocessing import Pool
from prediction_with_api.app import create_app


def start_server(host: str = "0.0.0.0", port: int = 5007, workers: int = 1):
    app = create_app()
    log.info(f"Running {workers} server processes on {host}:{port}")

    bjoern.listen(wsgi_app=app, host=host, port=port, reuse_port=True)

    if workers > 1:
        with Pool(processes=workers) as pool:
            mult_res = [pool.apply_async(bjoern.run) for _ in range(workers)]
            try:
                for res in mult_res:
                    res.wait()
            except (KeyboardInterrupt, SystemExit):
                pass
    else:
        try:
            bjoern.run()
        except (KeyboardInterrupt, SystemExit):
            pass
    log.info("Shutting server down")


def cli():
    fire.Fire(start_server)
