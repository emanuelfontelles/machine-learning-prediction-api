from flask import Flask, request
from loguru import logger as log
from prediction_with_api.api.customer import get_blueprint
from prediction_with_api.configuration import get_config


def log_requests(resp):
    log.debug(
        f"{request.remote_addr} {request.method} {request.full_path} "
        f"- {resp.status}"
    )
    return resp


def create_app() -> Flask:
    cfg = get_config()
    log.info(f"Creating app with {cfg}")

    app = Flask(__name__)
    app.config.from_object(cfg)
    app.after_request(log_requests)
    app.register_blueprint(get_blueprint())
    return app
