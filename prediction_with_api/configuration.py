from dataclasses import dataclass
from os import environ
from toolz import memoize


DEFAULT_SETTINGS = "development"
ENV_SETTINGS_VAR = "APP_SETTINGS"


@dataclass
class Base:
    customer_AUTH_TOKEN: str = "dev"
    customer_ASYNC_DATA_PATH: str = "./tests/async_sample.parquet"
    customer_ASYNC_SCHEDULE: str = "* * * * *"


@dataclass
class Development(Base):
    pass


class Testing(Base):
    pass


class Production(Base):
    customer_AUTH_TOKEN: str = environ.get("API_customer_AUTH_TOKEN", "")
    customer_ASYNC_DATA_PATH: str = environ.get(
        "API_customer_ASYNC_DATA_PATH",
        "s3://datarisk-customer/async_aggregation/customer_async_agg_table",
    )
    customer_ASYNC_SCHEDULE: str = environ.get(
        "API_customer_ASYNC_SCHEDULE", "40 9 * * *"
    )


@memoize
def get_config():
    app_settings = environ.get(ENV_SETTINGS_VAR, DEFAULT_SETTINGS).lower()
    if app_settings == "development":
        return Development
    elif app_settings == "testing":
        return Testing
    elif app_settings == "production":
        return Production
    else:
        raise Exception(f"No configuration for {ENV_SETTINGS_VAR}={app_settings}")
