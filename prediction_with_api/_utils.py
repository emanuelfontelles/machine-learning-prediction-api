from datetime import datetime as dt
from croniter import croniter
from functools import wraps
from threading import Thread, Lock
from typing import Dict, Any


def cache_with_schedule(schedule: str):
    """
    Decorator to cache the results of a function "f(x)" and clear the cache as
    scheduled.

    -> "schedule" is a cron-like string.
    -> a "dict" is used as cache, so "x" needs to be hashable.

    Example:

    # "func" is invoked at most once in a minute.
    @cache_with_schedule("*/1 * * * *")
    def func(n):
        print("I was invoked")
        return {"a": n, "b": 20}
    """
    nowfn = dt.utcnow
    cron = croniter(schedule, nowfn())
    cache: Dict[Any, Any] = dict()
    fetch_thread: Thread
    # To lock the access to the shared "cache" object.
    cache_lock = Lock()

    def has_expired() -> bool:
        """
        Returns "True" if the cache has expired.
        """
        now = nowfn()
        has_expired = False
        while now > cron.get_next(dt):
            has_expired = True
        # Rewind one step to get the correct "next" date-time next time
        # "has_expired" is invoked.
        cron.get_prev(dt)
        return has_expired

    def refresh_cache(f, x):
        """
        Calls "f(x)", locks the shared "cache" object and update it. This
        function should be executed in another thread.
        """
        nonlocal cache
        val = f(x)
        with cache_lock:
            cache[x] = val

    def decorator(f):
        @wraps(f)
        def wrapper(x):
            """
            Caches "f(x)" and checks for cache expiration. When the cache is
            expired, the current value is returned and a execution of "f(x)" is
            done in another thread.
            """
            nonlocal cache, fetch_thread
            res: Any

            if x in cache:
                with cache_lock:
                    res = cache[x]
                if has_expired():
                    fetch_thread = Thread(target=refresh_cache, args=(f, x))
                    fetch_thread.start()
            else:
                res = f(x)
                with cache_lock:
                    cache[x] = res
            return res

        return wrapper

    return decorator
