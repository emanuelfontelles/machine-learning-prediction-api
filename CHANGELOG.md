# Changelog

## [0.1.0] - 2020-03-24

### Added

- API endpoint "credit-v1" with input/output formats defined.
- Requests authorization.
- Complete Dockerfile.
- Description of the API schema using GET method.
- bjoern as the web server.
